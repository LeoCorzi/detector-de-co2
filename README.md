![status: ok](https://img.shields.io/badge/status-oK-green) ![hver: prototipo](https://img.shields.io/badge/hver-prototipo-green)

# Sistema de monitoreo de CO2 basado en ESP8266

El sistema desarrollado permite el monitoreo de la concentración de dióxido de carbono en el aire a partir del sensor Mh-z19, y la temperatura y la humedad ambiental mediante el sensor DHT22.

Aprovechando las capacidades IoT con las que cuenta la plataforma, los datos obtenidos se envían a los servidores de ThingSpeak cada 60 segundos para su posterior análisis. 

Para el control del dispositivo y la visualización de los datos, el sistema cuenta con una interfaz de usuario. Esta interfaz se encuentra formada por un servidor web embebido en la ESP8266, una pantalla LCD y diodos de alerta tipo semáforo.

<img src="https://drive.google.com/uc?export=view&id=12zCKkEUyJH82N7SLOfQPIvdWqYNGLHbA" alt="Dispositivo Instalado" width="50%">

<br/>

---

## Motivación
Este proyecto tiene como objetivo el desarrollo de un dispositivo IoT para la medición de la concentración de CO2, siendo esta medición un parámetro directamente relacionado con la necesidad de ventilar en espacios cerrados. De esta manera, este dispositivo se presenta como una herramienta útil para colaborar con las tareas destinas a evitar la propagación del COVID-19 y garantizar la seguridad del personal del Centro Atómico Bariloche, perteneciente a la Comisión Nacional de Energía Atómica.

<br/>

---

## Características del sistema

* Lectura de los sensores Mh-z19 y DHT22.
* Conexión a red wifi y sincronizarían de los datos con ThingSpeak mediante HTTP.
* Configuración mediante una pagina web embebida disponible permanentemente por Access Point.
* Almacenamiento de las configuraciones en memoria no volátil.
* Visualiza los datos obtenidos en una pantalla LCD e indicadores tipo semáforo.
* Alimentación mediante USB.

<br/>

---

## Hardware
El diseño de la placa PCB fue desarrollado en el software libre KiCad y todos los archivos generados se encuentran en la carpeta correspondiente.

Los principales componentes del sistema son los siguientes:
* Plataforma ESP8266 en NodeMCU_1.0_(ESP-12E).
* Sensor de CO2 Mh-z19.
* Sensor de temperatura y humedad DHT22.
* Pantalla LCD 16x2 I2C.
* Diodos indicadores tipo semáforo.

Ademas la placa cuenta con la capacidad para instalar entradas y salidas adicionales, las cuales deben ser implementadas en la programación de la plataforma:
* Dispositivos I2C x2
* Salida para control de carga mediante transistor.
* Sensor de gas MQ-135.
* Led de control.
* Botones x3.

<img src="https://drive.google.com/uc?export=view&id=1UlPRDJcf86lXLyc_7Fj8L65BlrVs6vPL" alt="Hardware del Sistema" width="50%">

<br/>

---

## Software
El software del sistema fue desarrollado utilizando el IDE de Arduino y todos los archivos generados se encuentran en la carpeta correspondiente.

Las principales librerías utilizadas son:
* General: `Arduino.h`
* Mh-z19: `MHZ19.h`, `SoftwareSerial.h`
* DHT22: `DHT.h`, `Wire.h`
* Display: `LiquidCrystal_I2C.h`
* Wifi: `ESP8266WiFi.h`, `ESP8266WebServer.h`
* Almacenamiento: `EEPROM.h`

<img src="https://drive.google.com/uc?export=view&id=1s8tmSfeyU3w5wdW-6szl7Kke5WKlup5U" alt="Canal de ThinkSpeak" width="50%">

<br/>

---

## Configuración
Para configurar el dispositivo, se debe ingresar a la pagina web embebida utilizando la siguiente informacion:
* Nombre de Red AP: AirQ_xxx
* Contraseña: 14cnea78
* Direccion IPv4: 192.168.4.1

<br/>
---

## Colaborar
En caso de querer colaborar con el desarrollo, ponerse en contacto con el administrador.
* damian.corzi@ib.edu.ar
* mariano.berisso@ib.edu.ar 

<br/>

---

## Licencias
Este repositorio es público y tiene licencia Creative Commons.

<br/>

---

## Dispositivos Asignados
Los siguientes dispositivos fueron asignados a modo de ejemplo.

| Nombre | URL | Write API Key | Cuenta |
|-|-|-|-|
| D01 | https://thingspeak.com/channels/1784774 | 3JAJ1HXPVCUU89NN | lipo@ib.edu.ar |
| OFI | https://thingspeak.com/channels/1784833 | B4CQRLPJEO6E4OD8 | lipo@ib.edu.ar |
| Dx3 | https://thingspeak.com/channels/1965733 | A33XIF67534R56Q7 | lipo@ib.edu.ar |
