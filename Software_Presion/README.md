# Software
La programación se desarrollo utilizando el IDE de Arduino. El archivo principal se encuentra en esta carpeta bajo el nombre de "Software.ino".

## Tareas realizadas por el programa
* Lectura de los sensores Mh-z19 y DHT22.
* Conexión a red wifi y sincronizarían de los datos con ThingSpeak mediante HTTP.
* Configuración mediante una pagina web embebida disponible permanentemente por Access Point.
* Almacenamiento de las configuraciones en memoria no volátil.
* Visualización los datos obtenidos en una pantalla LCD e indicadores tipo semáforo.

## Librerías utilizadas
* General: `Arduino.h`
* Mh-z19: `MHZ19.h`, `SoftwareSerial.h`
* DHT22: `DHT.h`, `Wire.h`
* Display: `LiquidCrystal_I2C.h`
* Wifi: `ESP8266WiFi.h`, `ESP8266WebServer.h`
* Almacenamiento: `EEPROM.h`

## Distribución del programa
* Definición de constantes globales utilizadas por el sistema.
* Configuración inicial durante `setup` de:
    * Memoria FLASH, lectura de parámetros almacenados.
    * Periféricos, sensores e indicadores.
    * Redes y servidor web embebido.
    * Variables globales.
* Tareas realizadas en el bucle principal `loop`:
    * Adquisición de valores medidos por sensores.
    * Actualización de la información en indicadores.
    * Envío de los datos a servidores de ThingSpeak.
    * Manejo de solicitudes realizadas al servidor web embebido.
