#include <Arduino.h>
#include "MHZ19.h"
#include <SoftwareSerial.h>
#include "DHT.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>
#include <Adafruit_BMP280.h>

//Descomentar esta linea para habilitar
//el envio de mensajes al puerto serie.
//Durante la depuracion, los LEDs indicadores
//no estan habilitados.
//#define DEPURAR

//Sensor de CO2
#define MHZ19_RX_PIN        D5
#define MHZ19_TX_PIN        D6
#define MHZ19_BAUDRATE      9600
MHZ19 myMHZ19;                               
SoftwareSerial mySerial(MHZ19_RX_PIN, MHZ19_TX_PIN);
float MHZ19_CO2_0 = 0;

//Sensor de humedad y temperatura
#define DHT_TYPE            DHT22
#define DHT_PIN             D3
#define DHT_NMED            10
DHT dht(DHT_PIN, DHT_TYPE);
float DHT22_t_0 = 0;
float DHT22_h_0 = 0;

//Sensor de presion
Adafruit_BMP280 bmp;
#define SEALEVELPRESSURE_HPA (1013.25)
float BMP280_pre_0 = 0;
float BMP280_tem_0 = 0;

//Display
#define LCD_ADD             0x27
#define LCD_CHR             16
#define LCD_LIN             2
LiquidCrystal_I2C lcd(LCD_ADD, LCD_CHR, LCD_LIN);

//Leds
//Debido a que existen dos configuraciones diferentes,
//la ubicacion de los leds puede cambiar.
#ifndef DEPURAR
#define LEDV 1
#define LEDA 3
#define LEDR 15
#endif

//Umbral de medicion
#define UMBBAJO 600
#define UMBALTO 1000

//Parametros para Wifi AP
#define AP_SSID "AirQ_"
#define AP_PASS "14cnea78"
ESP8266WebServer server(80);
bool STA_RED = false;

//Parametros para Wifi STA
String STA_SSID = "xxxxxxxx";
String STA_PASS = "xxxxxxxx";
WiFiClient  client;

//Parametros de ThingSpeak
String TKS_API = "xxxxxxxxxxxxxxxx";

//Tiempo de muestreo
#define SPL_TIME         1000
unsigned long getDataTimer = 0;

//Tiempo de actualizacion
//y variables auxiliares
#define UPD_TIME         120
uint8_t updat = 0;
float acu_CO2 = 0;
float acu_TEM = 0;
float acu_HUM = 0;
float acu_PRE = 0;
float acu_TE2 = 0;

//Varible de impresion en pantalla
#define IMPt              5
uint8_t impr  = 0;

//Numero de identificacion del Equipo
String AQ_ID = "xxx";

//Valor a escribir durante el primer inicio,
//determina el valor incial de los parametros
//de configuracion en la funcion first_Start
#define CBYTE             179

void setup()
{ 
  delay(100);
  
  //Consulta si se trata del primer inicio,
  //caso contrario carga los valores previos
  first_Start();
  
  delay(100);

#ifdef DEPURAR
  Serial.begin(115200);
  Serial.println("\n\n");
  Serial.println(F("##################################################"));
  Serial.println(F("#"));
  Serial.print(F("# ")); Serial.println(__FILE__);
  Serial.print(F("# compilado el ")); Serial.println( __DATE__ " , " __TIME__  );
  Serial.println(F("# por corzi.dll@gmail.com"));
  Serial.println(F("#"));
  Serial.println(F("##################################################"));

  Serial.print(F("# MHZ19_RX_PIN:   ")); Serial.println(MHZ19_RX_PIN);
  Serial.print(F("# MHZ19_TX_PIN:   ")); Serial.println(MHZ19_TX_PIN);
  Serial.print(F("# MHZ19_BAUDRATE: ")); Serial.println(MHZ19_BAUDRATE);

  Serial.print(F("# DHT_TYPE:       ")); Serial.println(DHT_TYPE);
  Serial.print(F("# DHT_PIN:        ")); Serial.println(DHT_PIN);
  Serial.print(F("# DHT_NMED:       ")); Serial.println(DHT_NMED);

  Serial.print(F("# BMP_SEALVLPRE:  ")); Serial.println(SEALEVELPRESSURE_HPA);

  Serial.print(F("# LCD_ADD:        ")); Serial.println(LCD_ADD);
  Serial.print(F("# LCD_CHR:        ")); Serial.println(LCD_CHR);
  Serial.print(F("# LCD_LIN:        ")); Serial.println(LCD_LIN);
  
  Serial.print(F("# AP_SSID:        ")); Serial.println(AP_SSID + AQ_ID);
  Serial.print(F("# AP_PASS:        ")); Serial.println(AP_PASS);

  Serial.print(F("# STA_SSID:       ")); Serial.println(STA_SSID);
  Serial.print(F("# STA_PASS:       ")); Serial.println(STA_PASS);

  Serial.print(F("# TKS_API:        ")); Serial.println(TKS_API);
  
  Serial.print(F("# SPL_TIME:       ")); Serial.println(SPL_TIME);
  Serial.print(F("# UPD_TIME:       ")); Serial.println(UPD_TIME);

  Serial.print(F("# DISPOSITIVO:    ")); Serial.println(AQ_ID);
  
  Serial.println(F("##################################################"));
  Serial.println("\n\n");
  
#endif

#ifndef DEPURAR
  //Leds semaforo
  pinMode(LEDV, OUTPUT);
  pinMode(LEDA, OUTPUT);
  pinMode(LEDR, OUTPUT);
#endif

  //Inicia LCD
  Wire.begin();
  lcd.init();
  lcd.clear();
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("Dispositivo "+ AQ_ID);

  //Inicia Sensor CO2
  mySerial.begin(MHZ19_BAUDRATE);
  myMHZ19.begin(mySerial);
  myMHZ19.autoCalibration();

  //Espera necesaria
  delay(1500);

  //Inicia Sensor Humedad y Temperatura
  dht.begin();

  //Inicia Sensor de Presion
  bmp.begin(0x76);
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X1,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */
  
  //Inicia Wifi
  //Configura para trabajar en ambos modos
  WiFi.mode(WIFI_AP_STA);
  //Datos del punto de acceso
  WiFi.softAP(AP_SSID+AQ_ID, AP_PASS);
  //Trata de contectar a la red especificada
  WiFi.begin(STA_SSID, STA_PASS);

  lcd.setCursor(0,0);
  lcd.print("Conectando...   ");

#ifdef DEPURAR
  Serial.println(F("Conectando. SSID: ") + STA_SSID + F(" PASS: ") + STA_PASS);
#endif

  //Espera a que la conexion haya sido exitosa durante 10 segudos
  uint8_t cont = 0;
  while (WiFi.status() != WL_CONNECTED)
  {
    if( cont > 100){
      break;
    }
    else{
      delay(100);
#ifdef DEPURAR
      Serial.print(F("."));
#endif
      cont++;
    }
  }

  //En caso de que la conexion no haya podido realizarse
  //solo crea el punto de acceso y enciende el servidor
  if(WiFi.status() != WL_CONNECTED){
#ifdef DEPURAR
    Serial.println(F("\nConexion fallo."));
#endif
    
    //Muestra estado en LCD
    lcd.setCursor(0,0);
    lcd.print("Conexion fallo.     ");
    delay(1000);
    
    //Cambia a modo AP
    WiFi.mode(WIFI_AP);
    WiFi.softAP(AP_SSID+AQ_ID, AP_PASS);

    //Flag de conexion exitosa
    STA_RED = false;
  }
  else{
    
#ifdef DEPURAR
    Serial.print("\nConexion lista.\nSTA IP: ");
    Serial.println(WiFi.localIP());
#endif
    
    //Muestra estado en LCD
    lcd.setCursor(0,0);
    lcd.print("Conexion lista.     ");
    delay(1000);

    //Flag de conexion exitosa
    STA_RED = true;
  }

  //Informa Direccion IP de Punto de Acceso
#ifdef DEPURAR
  Serial.print("AP IP : ");
  Serial.println(WiFi.softAPIP());
#endif

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("AP: " + String(AP_SSID+AQ_ID));
  lcd.setCursor(0,1);
  lcd.print(WiFi.softAPIP());

  //Inicia Servidor Interno
  server.on("/", handle_Main);
  server.on("/update", handle_Update);
  server.onNotFound(handle_NotFound);
  server.begin();
  
#ifdef DEPURAR  
  Serial.println("Servidor AP listo.\n\nFin de las configuraciones.\n\n");
#endif

  //Una espera para que la informacion se
  //muestre en pantalla
  delay(3000);

#ifndef DEPURAR
  //Prueba conexion de leds
  digitalWrite(LEDV, HIGH);
  delay(150);
  digitalWrite(LEDA, HIGH);
  delay(150);
  digitalWrite(LEDR, HIGH);
  delay(150);
  digitalWrite(LEDR, LOW);
  delay(150);
  digitalWrite(LEDA, LOW);
  delay(150);
  digitalWrite(LEDV, LOW);
  delay(150);
#endif

  //Elimina toda la info en LCD
  //lcd.clear();

  //Reinicia acumuladores
  updat   = 0;
  acu_CO2 = 0;
  acu_TEM = 0;
  acu_HUM = 0;
  acu_PRE = 0;
  acu_TE2 = 0;
}

void loop()
{
  //Adquisicion de datos
  if (millis() - getDataTimer >= SPL_TIME){
    getDataTimer = millis();

    //Medicion temperatura y humedad.
    //Realiza varias mediciones y luego
    //promedia el valor para reducir el ruido
    float DHT22_t = 0;
    float DHT22_h = 0;
    for (int i = 0; i < DHT_NMED; i++) {
      DHT22_t += dht.readTemperature();
      DHT22_h += dht.readHumidity();
    }
    DHT22_t /= DHT_NMED;
    DHT22_h /= DHT_NMED;

    //Medicion de CO2
    float MHZ19_CO2 = myMHZ19.getCO2();

    //Medicion de presion
    float BMP280_pre = bmp.readPressure()/100;

    //Medicion de temperatura secundaria
    float BMP280_tem = bmp.readTemperature();
    
    //Actualiza valores si son correctos y diferentes
    //luego los visualiza en el LCD
    if( (isnan(DHT22_t) == false) && (DHT22_t != DHT22_t_0) ){
      DHT22_t_0 = DHT22_t;
    }

    //Actualiza valores si son correctos y diferentes
    if( (isnan(DHT22_h) == false) && (DHT22_h != DHT22_h_0) ){
      DHT22_h_0 = DHT22_h;
    }
    
    //Muestra la informacion en pantalla a los IMPt 
    if (impr == (IMPt - 1) ){
      lcd.setCursor(0,0);
      lcd.print("TEM: " + String(DHT22_t_0) + " C     ");
      lcd.setCursor(0,1);
      lcd.print("HUM: " + String(DHT22_h_0) + " %     ");
    }

    //Actualiza valores si son correctos y diferentes
    if( (MHZ19_CO2 > 0) && (MHZ19_CO2 != MHZ19_CO2_0) ){
       MHZ19_CO2_0 = MHZ19_CO2;

#ifndef DEPURAR
      //Semaforo
      if(MHZ19_CO2 < UMBBAJO){
        digitalWrite(LEDV, HIGH);
        digitalWrite(LEDA, LOW);
        digitalWrite(LEDR, LOW);
      }
      else if(MHZ19_CO2 > UMBALTO){
        digitalWrite(LEDV, LOW);
        digitalWrite(LEDA, LOW);
        digitalWrite(LEDR, HIGH);
      }
      else{
        digitalWrite(LEDV, LOW);
        digitalWrite(LEDA, HIGH);
        digitalWrite(LEDR, LOW);
      }
#endif
    }

    //Actualiza valores si son correctos y diferentes
    if( (isnan(BMP280_pre) == false) && (BMP280_pre != BMP280_pre_0) ){
      BMP280_pre_0 = BMP280_pre;
    }

    //Actualiza valores si son correctos y diferentes
    if( (isnan(BMP280_tem) == false) && (BMP280_tem != BMP280_tem_0) ){
      BMP280_tem_0 = BMP280_tem;
    }

    //Muestra la informacion en pantalla a los 2*IMPt
    if (impr == (2*IMPt - 1) ){
        lcd.setCursor(0,0);
        lcd.print("CO2: " + String(int(MHZ19_CO2_0)) + " ppm    ");
        lcd.setCursor(0,1);
        lcd.print("PRE: " + String(int(BMP280_pre_0)) + " hPa   ");
      }

    //Si llego al maximo del tiempo, 
    if (impr >= (2*IMPt - 1) ){
      impr = 0;
    }
    else{
      impr = impr + 1;
    }

    //Si se pudo conectar a la red wifi
    if(STA_RED){
      //Calculo valores medios que seran enviados a ThingSpeak
      acu_CO2 = acu_CO2 + MHZ19_CO2_0;
      acu_TEM = acu_TEM + DHT22_t_0;
      acu_HUM = acu_HUM + DHT22_h_0;
      acu_PRE = acu_PRE + BMP280_pre_0;
      acu_TE2 = acu_TE2 + BMP280_tem_0;
      
      //Incrementa la actualizacion
      updat++;
    }

#ifdef DEPURAR
    //Envio de informacion por puerto Serie
    Serial.print(int(getDataTimer / 1000.0));
    Serial.print("\t");
    Serial.print(updat);
    Serial.print("\t");
    Serial.print(MHZ19_CO2_0);
    Serial.print("\t");
    Serial.print(DHT22_t_0);
    Serial.print("\t");
    Serial.print(DHT22_h_0);
    Serial.print("\t");
    Serial.println(BMP280_pre_0);
#endif

  }

  //Actualizacion de datos en el servidor externo
  if(updat >= UPD_TIME){

    //Si esta conectado a una red, envia los datos al servidor
    if(check_WIFI()){
      //Enviar datos a ThingSpeak
      updateThingSpeak(acu_CO2/UPD_TIME, acu_TEM/UPD_TIME, acu_HUM/UPD_TIME, acu_PRE/UPD_TIME, acu_TE2/UPD_TIME);
    }
    
    //Reinicia acumuladores
    acu_CO2 = 0;
    acu_TEM = 0;
    acu_HUM = 0;
    acu_PRE = 0;
    acu_TE2 = 0;
    updat   = 0;
  }

  //Manejo de las solicitudes del servidor AP
  server.handleClient();

}

//Funcion que envia los datos a los servidores
//de ThingSpeak mediante el metodo POST de HTTP
void updateThingSpeak(float CO2, float TEMP, float HUM, float PRE, float TE2) {
  if (client.connect("api.thingspeak.com", 80)) {
    String tsData = "field1=" + String(CO2) + "&field2=" + String(TEMP) + "&field3=" + String(HUM) + "&field4=" + String(PRE) + "&field5=" + String(TE2);
    client.print("POST /update HTTP/1.1\n");
    client.print("Host: api.thingspeak.com\n");
    client.print("Connection: close\n");
    client.print("X-THINGSPEAKAPIKEY: " + TKS_API + "\n");
    client.print("Content-Type: application/x-www-form-urlencoded\n");
    client.print("Content-Length: ");
    client.print(tsData.length());
    client.print("\n\n");
    client.print(tsData);
    client.print("\n");

#ifdef DEPURAR
    if (client.connected()) {
      Serial.println("\nDatos subidos: " + tsData +"\n");
    }
#endif
  }
}

//Funcion que genera la pagina principal
//de configuracion del dispositivo
void handle_Main() {
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta charset=\"utf-8\"/> <style> h1,h3,fieldset,p,div,form {text-align: center;} </style>";
  ptr +="<meta name=\"viewport\" content=\"width=device-width\"></head>";
  ptr +="<body style=\"background-color:#E4F6FF\">";
  ptr +="<h1>AirQuality</h1>";
  ptr +="<h3>Dispositivo " + AQ_ID + "</h3><br>";
  ptr +="<form action=\"/update\" method=\"POST\">";
  ptr +="<fieldset><legend>Red Wi-Fi</legend>";
  ptr +="<label for=\"name\">Nombre:</label><br>";
  ptr +="<input type=\"text\" id=\"SSID\" name=\"SSID\" pattern=\"{1,32}\"><br><br>";
  ptr +="<label for=\"pass\">Contraseña:</label><br>";
  ptr +="<input type=\"text\" id=\"PASS\" name=\"PASS\"><br></fieldset><br>";
  ptr +="<fieldset><legend>ThingSpeak</legend>";
  ptr +="<label for=\"api\">API key:</label><br>";
  ptr +="<input type=\"text\" id=\"API\" name=\"API\" pattern=\"{16}\"><br></fieldset><br>";
  ptr +="<fieldset><legend>Dispositivo</legend>";
  ptr +="<label for=\"idd\">Numero:</label><br>";
  ptr +="<input type=\"text\" id=\"ID\" name=\"IDD\" pattern=\"[A-Za-z0-9]{3}\"><br></fieldset><br>";
  ptr +="<input type=\"submit\" value=\"Cargar\">";
  ptr +="</form>";
  ptr +="<p>Click en \"Cargar\" para actualizar la informacion.<br>";
  ptr +="En caso de que la red Wi-Fi no tenga contraseña, anotar un espacio vacio con la barra espaciadora.</p>";
  ptr +="</body>";
  ptr +="</html>";
  server.send(200, "text/html", ptr);
  
#ifdef DEPURAR 
  Serial.println("Main Page");
#endif
}

//Funcion que recibe los datos actualizados
//por el usuario, genera una pagina que indica
//los cambios realizados, actualiza la informacion en
//la memoria FLASH y reinicia el dispositivo
void handle_Update(){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head> <style> h1,p {text-align: center;} </style>";
  ptr +="<meta name=\"viewport\" content=\"width=device-width\"></head>";
  ptr +="<body style=\"background-color:#E4F6FF;\">";
  ptr +="<h1>AirQuality</h1>";
  
  ptr +="<p>Lista de datos actualizados:</p>";

  if (server.arg("SSID") != NULL){
    STA_SSID = server.arg("SSID");
    ptr +="<p>-SSID</p>";
    
    write_FLASH(STA_SSID,0);
    delay(50);
  }

  if (server.arg("PASS") != NULL){
    
    if(server.arg("PASS") != " "){
      STA_PASS = server.arg("PASS");
    }
    else{
      STA_PASS = "";
    }
    ptr +="<p>-PASS</p>";
    
    write_FLASH(STA_PASS,33);
    delay(50);
  }

  if (server.arg("API") != NULL){
    TKS_API = server.arg("API");
    ptr +="<p>-API</p>";

    write_FLASH(TKS_API,66);
    delay(50);
  }

  if (server.arg("IDD") != NULL){
    AQ_ID = server.arg("IDD");
    ptr +="<p>-IDD</p>";

    write_FLASH(AQ_ID, 85);
    delay(50);
  }

  ptr +="<p>El dispositivo se reiniciara automaticamente.</p>";
  
  ptr +="</body>";
  ptr +="</html>";
  
  server.send(200, "text/html", ptr);
  delay(100);

#ifdef DEPURAR
  Serial.println("Datos almacenados:");
  Serial.println("- " + read_FLASH(0));
  Serial.println("- " + read_FLASH(33));
  Serial.println("- " + read_FLASH(66));
  Serial.println("- " + read_FLASH(85));
  
  Serial.println("Reset...\n\n\n\n\n\n\n\n");
#endif

  lcd.setCursor(0,0);
  lcd.print("Reiniciando...  ");
  delay(200);
  
  ESP.reset();
}

//Funcion que genera el mensaje de error
void handle_NotFound(){
  server.send(404, "text/plain", "Not found");
  
#ifdef DEPURAR
  Serial.println("Error 404");
#endif
}

//Funcion que determina si se trata del
//primer inicio del sistema. En caso afirmativo,
//configura la memoria FLASH para almacenar en ella
//los parametros configurables.
//En caso contrario, lee de la memoria FLASH
//los parametros almacenados.
bool first_Start(){
  //Cantidad de memoria
  int CM = 128;
  
  EEPROM.begin(CM);

  uint8_t valor;
  EEPROM.get(CM-1, valor);

  //Si se trata de un nuevo inicio
  if( valor == CBYTE ){
    STA_SSID = read_FLASH(0);
    STA_PASS = read_FLASH(33);
    TKS_API  = read_FLASH(66);
    AQ_ID    = read_FLASH(85);
    return true;
  }
  else{
    write_FLASH(STA_SSID,0);
    write_FLASH(STA_PASS,33);
    write_FLASH(TKS_API,66);
    write_FLASH(AQ_ID, 85);
    EEPROM.put(CM-1, uint8_t(CBYTE) );
    EEPROM.commit();
    return false;
  }
 
}

//Funcion para escribir informacion en la
//memoria FLASH del dispositivo
void write_FLASH(String Data, uint8_t offset){
  uint8_t Cant = Data.length();

  for( uint8_t i = 0; i < Cant; i++){
    EEPROM.put(i + offset, Data[i]);
  }

  EEPROM.put(offset + Cant, uint8_t('!'));
  EEPROM.commit();
}

//Funcion para leer informacion de la
//memoria FLASH del dispositivo
String read_FLASH(uint8_t offset){
  String Buffer;
  char valor;

  for( uint8_t i = 0; i < 32; i++){
    EEPROM.get(i+offset, valor);
    
    if(valor != '!'){
      Buffer += valor;
    }
    else{
      break;
    }
  }
  
  return Buffer;
}


//Funcion para verificar si esta conectado a internet
//y en caso contrario, realizar la conexion
bool check_WIFI(){
    
    if( WiFi.status() != WL_CONNECTED ){
      //Trata de contectar a la red especificada
      int status = WiFi.begin(STA_SSID, STA_PASS);

      if( WiFi.status() != WL_CONNECTED ){
        return false;
      }
      else{
        return true;
      }
      
    }
    else{
      return true;
    }

}
