## Hardware
La placa PCB fue diseñada utilizando el software libre KiCad. En esta carpeta se encuentran todos lo archivos necesarios editar el proyecto a partir del archivo principal "AirQualityPCB.pro".

Los principales componentes del sistema son los siguientes:
* Plataforma ESP8266 en NodeMCU_1.0_(ESP-12E).
* Sensor de CO2 Mh-z19.
* Sensor de temperatura y humedad DHT22.
* Pantalla LCD 16x2 I2C.
* Diodos indicadores tipo semáforo.

Ademas la placa cuenta con la capacidad para instalar entradas y salidas adicionales, las cuales deben ser implementadas en la programación de la plataforma:
* Dispositivos I2C x2
* Salida para control de carga mediante transistor.
* Sensor de gas MQ-135.
* Led de control.
* Botones x3.

<img src="https://drive.google.com/uc?export=view&id=1UlPRDJcf86lXLyc_7Fj8L65BlrVs6vPL" alt="Hardware del Sistema" width="50%">
